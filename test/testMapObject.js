const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const mapObject = require('../mapObject')

const result = mapObject(testObject, (name) => {
    name = name + " Modified"
    return name;
})

console.log(result)