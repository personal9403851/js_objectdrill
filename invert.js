const invert = (obj) => {

    const result = {}
    for (const key in obj) {

        let Key = key;
        let value = obj[key]
        result[value] = Key
    }
    return result;
}

module.exports = invert;