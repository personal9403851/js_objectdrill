const mapObject = (obj, cb) => {
    const result = {}
    for (const key in obj) {
        result[key] = cb(obj[key])
    }
    return result;
}

module.exports = mapObject;