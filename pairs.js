const _ = require("./test/node_modules/underscore")

const pairs = (obj) => {
    return _.pairs(obj)
}

module.exports = pairs;